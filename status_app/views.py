from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Form
from .models import Status_Form
from django.db import IntegrityError

# Create your views here.
response = {}

def index(request):
	result = Status_Form.objects.all()
	if request.method == 'POST':
		form = Form(request.POST)
		if form.is_valid():
			status = request.POST['status']
			create_stat = Status_Form.objects.create(status=status) 
			return render(request, 'index.html', {'form' : form, 'status':result})
	else:
		form = Form()
	return render(request, 'index.html', {'form' : form, 'status':result})

# def delete(request):
#     form = Form()
#     result = Status_Form.objects.all()
#     status = Status_Form.objects.all().delete()
#     return render(request, 'index.html', {'form' : form, 'status':result})

